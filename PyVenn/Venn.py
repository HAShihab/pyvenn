# coding: utf-8
# https://github.com/tctianchi/pyvenn

import math
import matplotlib.pyplot as plt
import matplotlib.patches as patches

from itertools import chain
from collections import Iterable
from matplotlib import colors

default_colors = [
    # r, g, b, a
    [92, 192, 98, 0.5],
    [90, 155, 212, 0.5],
    [246, 236, 86, 0.6],
    [241, 90, 96, 0.4],
    [255, 117, 0, 0.3],
    [82, 82, 190, 0.2],
]
default_colors = [
    [i[0] / 255.0, i[1] / 255.0, i[2] / 255.0, i[3]]
    for i in default_colors
]

def draw_ellipse(fig, ax, x, y, w, h, a, fillcolor):
    e = patches.Ellipse(
        xy=(x, y),
        width=w,
        height=h,
        angle=a,
        color=fillcolor)
    ax.add_patch(e)

def draw_triangle(fig, ax, x1, y1, x2, y2, x3, y3, fillcolor):
    xy = [
        (x1, y1),
        (x2, y2),
        (x3, y3),
    ]
    polygon = patches.Polygon(
        xy=xy,
        closed=True,
        color=fillcolor)
    ax.add_patch(polygon)
    
def draw_text(fig, ax, x, y, text, color=[0, 0, 0, 1], fontsize=14):
    ax.text(
        x, y, text,
        horizontalalignment='center',
        verticalalignment='center',
        fontsize=fontsize,
        color=color)
    
def draw_annotate(fig, ax, x, y, textx, texty, text, color=[0, 0, 0, 1], arrowcolor=[0, 0, 0, 0.3]):
    plt.annotate(
        text,
        xy=(x, y),
        xytext=(textx, texty),
        arrowprops=dict(color=arrowcolor, shrink=0, width=0.5, headwidth=8),
        fontsize=14,
        color=color,
        xycoords="data",
        textcoords="data",
        horizontalalignment='center',
        verticalalignment='center'
    )

def get_intersects(data, fill="number"):
    """    
    get a dict of intersections for groups in data
    
    @type data: list[Iterable]    
    @rtype: dict[str, str]

    input
      data: data to get label for
      fill: one of "number"|"percent"|"both"

    return
      intersects: a dict of intersects for different sets

    example:
    In [12]: get_intersects([range(10), range(5,15), range(3,8)], fill=["number"])
    Out[12]:
    {'001': '0',
     '010': '5',
     '011': '0',
     '100': '3',
     '101': '2',
     '110': '2',
     '111': '3'}
    """

    N = len(data)

    sets_data = [set(data[i]) for i in range(N)]  # sets for separate groups
    s_all = set(chain(*data))                     # union of all sets

    # bin(3) --> '0b11', so bin(3).split('0b')[-1] will remove "0b"
    set_collections = {}
    for n in range(1, 2**N):
        key = bin(n).split('0b')[-1].zfill(N)
        value = s_all
        sets_for_intersection = [sets_data[i] for i in range(N) if key[i] == '1']
        sets_for_difference = [sets_data[i] for i in range(N) if key[i] == '0']
        for s in sets_for_intersection:
            value = value & s
        for s in sets_for_difference:
            value = value - s
        set_collections[key] = value

    intersects = { k: "" for k in set_collections }
    if fill == "number":
        for k in set_collections:
            intersects[k] += str(len(set_collections[k]))
    if fill == "percent":
        data_size = len(s_all)
        for k in set_collections:
            intersects[k] += "%.1f%%" % (100.0 * len(set_collections[k]) / data_size)
    if fill == "both":
        data_size = len(s_all)
        for k in set_collections:
            intersects[k] += str(len(set_collections[k]))
            intersects[k] += " (%.1f%%)" % (100.0 * len(set_collections[k]) / data_size)

    return intersects

def Venn2(data, names, **options):
    """
    plots a 2-set Venn diagram
        
    @type intersects: dict[str, str]
    @type names: list[str]
    @rtype: (Figure, AxesSubplot)
    
    input
      intersects: a label dict where keys are identified via binary codes ('01', '10', '11'),
              hence a valid set could look like: {'01': 'text 1', '10': 'text 2', '11': 'text 3'}.
              unmentioned codes are considered as ''.
      names:  group names
      more:   colors, figsize, dpi, fontsize

    return
      pyplot Figure and AxesSubplot object
    """
    
    assert len(data) == 2, "unexpected data shape (expecting 2 got {0})".format(len(data))
    assert len(names) == 2, "unexpected name shape (expecting 2 got {0})".format(len(names))
    
    fill = options.get('fill', 'number')
    intersects = get_intersects(data, fill=fill)
    
    show_labels = options.get('show_labels', False)
    show_legend = options.get('show_legend', True)
    
    colors = options.get('colors', [default_colors[i] for i in range(2)])
    figsize = options.get('figsize', (9, 7))
    dpi = options.get('dpi', 96)
    fontsize = options.get('fontsize', 14)
    
    fig = plt.figure(0, figsize=figsize, dpi=dpi)
    ax = fig.add_subplot(111, aspect='equal')
    ax.set_axis_off()
    ax.set_ylim(bottom=0.0, top=0.7)
    ax.set_xlim(left=0.0, right=1.0)
    
    # body
    draw_ellipse(fig, ax, 0.375, 0.3, 0.5, 0.5, 0.0, colors[0])
    draw_ellipse(fig, ax, 0.625, 0.3, 0.5, 0.5, 0.0, colors[1])
    draw_text(fig, ax, 0.74, 0.30, intersects.get('01', ''), fontsize=fontsize)
    draw_text(fig, ax, 0.26, 0.30, intersects.get('10', ''), fontsize=fontsize)
    draw_text(fig, ax, 0.50, 0.30, intersects.get('11', ''), fontsize=fontsize)
    
    # legend
    if show_labels:
        draw_text(fig, ax, 0.20, 0.56, names[0], colors[0], fontsize=fontsize)
        draw_text(fig, ax, 0.80, 0.56, names[1], colors[1], fontsize=fontsize)
    if show_legend:
        leg = ax.legend(names, loc='best', fancybox=True)
        leg.get_frame().set_alpha(0.5)
    
    return fig, ax

def Venn3(data, names, **options):
    """
    plots a 3-set Venn diagram
        
    @type intersects: dict[str, str]
    @type names: list[str]
    @rtype: (Figure, AxesSubplot)
    
    input
      intersects: a label dict where keys are identified via binary codes ('001', '010', '100', ...),
              hence a valid set could look like: {'001': 'text 1', '010': 'text 2', '100': 'text 3', ...}.
              unmentioned codes are considered as ''.
      names:  group names
      more:   colors, figsize, dpi, fontsize

    return
      pyplot Figure and AxesSubplot object
    """
    
    assert len(data) == 3, "unexpected data shape (expecting 2 got {0})".format(len(data))
    assert len(names) == 3, "unexpected name shape (expecting 2 got {0})".format(len(names))
    
    fill = options.get('fill', 'number')
    intersects = get_intersects(data, fill=fill)
    
    show_labels = options.get('show_labels', False)
    show_legend = options.get('show_legend', True)
    
    colors = options.get('colors', [default_colors[i] for i in range(3)])
    figsize = options.get('figsize', (9, 9))
    dpi = options.get('dpi', 96)
    fontsize = options.get('fontsize', 14)
    
    fig = plt.figure(0, figsize=figsize, dpi=dpi)
    ax = fig.add_subplot(111, aspect='equal')
    ax.set_axis_off()
    ax.set_ylim(bottom=0.0, top=1.0)
    ax.set_xlim(left=0.0, right=1.0)
    
    # body
    draw_ellipse(fig, ax, 0.333, 0.633, 0.5, 0.5, 0.0, colors[0])
    draw_ellipse(fig, ax, 0.666, 0.633, 0.5, 0.5, 0.0, colors[1])
    draw_ellipse(fig, ax, 0.500, 0.310, 0.5, 0.5, 0.0, colors[2])
    draw_text(fig, ax, 0.50, 0.27, intersects.get('001', ''), fontsize=fontsize)
    draw_text(fig, ax, 0.73, 0.65, intersects.get('010', ''), fontsize=fontsize)
    draw_text(fig, ax, 0.61, 0.46, intersects.get('011', ''), fontsize=fontsize)
    draw_text(fig, ax, 0.27, 0.65, intersects.get('100', ''), fontsize=fontsize)
    draw_text(fig, ax, 0.39, 0.46, intersects.get('101', ''), fontsize=fontsize)
    draw_text(fig, ax, 0.50, 0.65, intersects.get('110', ''), fontsize=fontsize)
    draw_text(fig, ax, 0.50, 0.51, intersects.get('111', ''), fontsize=fontsize)
    
    # legend
    if show_labels:
        draw_text(fig, ax, 0.15, 0.87, names[0], colors[0], fontsize=fontsize)
        draw_text(fig, ax, 0.85, 0.87, names[1], colors[1], fontsize=fontsize)
        draw_text(fig, ax, 0.50, 0.02, names[2], colors[2], fontsize=fontsize)
    if show_legend:
        leg = ax.legend(names, loc='best', fancybox=True)
        leg.get_frame().set_alpha(0.5)
    
    return fig, ax

def Venn4(data, names, **options):
    """
    plots a 4-set Venn diagram
        
    @type intersects: dict[str, str]
    @type names: list[str]
    @rtype: (Figure, AxesSubplot)
    
    input
      intersects: a label dict where keys are identified via binary codes ('0001', '0010', '0100', ...),
              hence a valid set could look like: {'0001': 'text 1', '0010': 'text 2', '0100': 'text 3', ...}.
              unmentioned codes are considered as ''.
      names:  group names
      more:   colors, figsize, dpi, fontsize

    return
      pyplot Figure and AxesSubplot object
    """
    
    assert len(data) == 4, "unexpected data shape (expecting 2 got {0})".format(len(data))
    assert len(names) == 4, "unexpected name shape (expecting 2 got {0})".format(len(names))
    
    show_labels = options.get('show_labels', False)
    show_legend = options.get('show_legend', True)
    
    fill = options.get('fill', 'number')
    intersects = get_intersects(data, fill=fill)
    
    colors = options.get('colors', [default_colors[i] for i in range(4)])
    figsize = options.get('figsize', (12, 12))
    dpi = options.get('dpi', 96)
    fontsize = options.get('fontsize', 14)
    
    fig = plt.figure(0, figsize=figsize, dpi=dpi)
    ax = fig.add_subplot(111, aspect='equal')
    ax.set_axis_off()
    ax.set_ylim(bottom=0.0, top=1.0)
    ax.set_xlim(left=0.0, right=1.0)
    
    # body   
    draw_ellipse(fig, ax, 0.350, 0.400, 0.72, 0.45, 140.0, colors[0])
    draw_ellipse(fig, ax, 0.450, 0.500, 0.72, 0.45, 140.0, colors[1])
    draw_ellipse(fig, ax, 0.544, 0.500, 0.72, 0.45, 40.0, colors[2])
    draw_ellipse(fig, ax, 0.644, 0.400, 0.72, 0.45, 40.0, colors[3])
    draw_text(fig, ax, 0.85, 0.42, intersects.get('0001', ''), fontsize=fontsize)
    draw_text(fig, ax, 0.68, 0.72, intersects.get('0010', ''), fontsize=fontsize)
    draw_text(fig, ax, 0.77, 0.59, intersects.get('0011', ''), fontsize=fontsize)
    draw_text(fig, ax, 0.32, 0.72, intersects.get('0100', ''), fontsize=fontsize)
    draw_text(fig, ax, 0.71, 0.30, intersects.get('0101', ''), fontsize=fontsize)
    draw_text(fig, ax, 0.50, 0.66, intersects.get('0110', ''), fontsize=fontsize)
    draw_text(fig, ax, 0.65, 0.50, intersects.get('0111', ''), fontsize=fontsize)
    draw_text(fig, ax, 0.14, 0.42, intersects.get('1000', ''), fontsize=fontsize)
    draw_text(fig, ax, 0.50, 0.17, intersects.get('1001', ''), fontsize=fontsize)
    draw_text(fig, ax, 0.29, 0.30, intersects.get('1010', ''), fontsize=fontsize)
    draw_text(fig, ax, 0.39, 0.24, intersects.get('1011', ''), fontsize=fontsize)
    draw_text(fig, ax, 0.23, 0.59, intersects.get('1100', ''), fontsize=fontsize)
    draw_text(fig, ax, 0.61, 0.24, intersects.get('1101', ''), fontsize=fontsize)
    draw_text(fig, ax, 0.35, 0.50, intersects.get('1110', ''), fontsize=fontsize)
    draw_text(fig, ax, 0.50, 0.38, intersects.get('1111', ''), fontsize=fontsize)
    
    # legend
    if show_labels:
        draw_text(fig, ax, 0.13, 0.18, names[0], colors[0], fontsize=fontsize)
        draw_text(fig, ax, 0.18, 0.83, names[1], colors[1], fontsize=fontsize)
        draw_text(fig, ax, 0.82, 0.83, names[2], colors[2], fontsize=fontsize)
        draw_text(fig, ax, 0.87, 0.18, names[3], colors[3], fontsize=fontsize)
    if show_legend:
        leg = ax.legend(names, loc='best', fancybox=True)
        leg.get_frame().set_alpha(0.5)
    
    return fig, ax
