# PyVenn

Note: I simply packaged https://github.com/tctianchi/pyvenn for my own selfish use and **did not write it myself!**.  

All intellectual credit must be given to https://github.com/tctianchi

### Usgae:

```python
from PyVenn import Venn2, Venn3, Venn4

p1 = range(1, 10, 1)
p2 = range(1, 20, 2)
p3 = range(1, 30, 3)
p4 = range(1, 40, 4)

Venn2([ p1, p2 ], [ "p1", "p2" ])
Venn3([ p1, p2, p3 ], [ "p1", "p2", "p3" ])
Venn4([ p1, p2, p3, p4 ], [ "p1", "p2", "p3", "p4" ])
```

### Installation

```sh
pip install --upgrade git+https://gitlab.com/HAShihab/pyvenn.git
```