from setuptools import setup, find_packages

setup(
    name="PyVenn",
    version="0.0.2",
    author="Hashem Shihab",
    author_email="",
    description="Venn Diagrams for Python",
    long_description=open('README.md').read(),
    long_description_content_type="text/markdown",
    url="",
    packages=find_packages(),
)